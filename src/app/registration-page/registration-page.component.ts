import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.scss']
})
export class RegistrationPageComponent implements OnInit {

  registrationForm: FormGroup;

  constructor(private router: Router,
              private formBuilder: FormBuilder) {

    this.registrationForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      bio: ['', [Validators.required, Validators.minLength(5)]]
    });

  }

  ngOnInit(): void {
  }

  submitRegistration() {
    this.router.navigate(['profile', this.registrationForm.value], {skipLocationChange: true});
  }

}
