import { Component, OnInit } from '@angular/core';
import {ProfileService} from "../services/profile.service";
import {EMPTY, Observable} from "rxjs";
import {ProfileModel} from "../model/profile.model";

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

  profile$: Observable<ProfileModel> = EMPTY;

  constructor(private profileService: ProfileService) { }

  ngOnInit(): void {
    this.profile$ = this.profileService.retrieveProfile();
  }

}
