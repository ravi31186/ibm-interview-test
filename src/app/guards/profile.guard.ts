import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import {Observable, of } from 'rxjs';
import {RegistrationService} from "../services/registration.service";
import {switchMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProfileGuard implements CanActivate {
  constructor(private registrationService: RegistrationService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const payload = route.params;
    return this.registrationService.submitRegistration(payload).pipe(
      switchMap((resp: any) => {
        return of(resp.success);
      })
    );
  }
}
