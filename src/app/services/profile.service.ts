import { Injectable } from '@angular/core';
import {Observable, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  retrieveProfileUrl = 'https://mocki.io/v1/611a3036-4420-48a5-b8da-9b461853cdd2';

  constructor(private http: HttpClient) { }

  public retrieveProfile(): Observable<any> {
    return this.http.get(this.retrieveProfileUrl).pipe(
      map(registrationData => registrationData),
      catchError(error => {
        return throwError(error);
      })
    );
  }
}
