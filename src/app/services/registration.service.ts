import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, map} from "rxjs/operators";
import {Observable, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  registrationUrl = 'https://mocki.io/v1/7f434df6-a4ac-4817-ab7c-dd39a564d01d';

  constructor(private http: HttpClient) { }

  public submitRegistration(payload: any): Observable<any> {
    return this.http.get(this.registrationUrl, payload).pipe(
      map((registrationData) => registrationData),
      catchError(error => {
        return throwError(error);
      })
    )
  }
}
