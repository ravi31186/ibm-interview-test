export interface ProfileModel {
  name: string;
  email: string;
  bio: string;
}
