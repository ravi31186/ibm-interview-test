import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RegistrationPageComponent} from "./registration-page/registration-page.component";
import {ProfilePageComponent} from "./profile-page/profile-page.component";
import {ProfileGuard} from "./guards/profile.guard";

// Note: We can do lazy loading of module if we have huge application
// here just doing eager loading as the application is small
const routes: Routes = [
  {
    path: 'registration',
    component: RegistrationPageComponent
  },
  {
    path: 'profile',
    component: ProfilePageComponent,
    canActivate: [ProfileGuard]
  },
  {
    path: '',
    redirectTo: '/registration',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
